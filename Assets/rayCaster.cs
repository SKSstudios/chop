﻿using UnityEngine;
using System.Collections;

public class rayCaster : MonoBehaviour {

    // radius should be derived from distance to arrow;
    public GameController control;
    public float radius;
    public float range;
    public float difficultyMult = 20;
    RaycastHit2D ray;

    // bit shifts a mask so that
    // only layer 8 (log layer) 
    // can be hit
    int logLayerMask = 1 << 8;
    
    private GameObject Arrow;

    // current speed of the arrow's movement
    private float currentSpeed;

    // time for the wave movement. Should be derived from 
    // difficutly.
    private float frequency;
    private float sinTime;
    private float cumulitiveTime;
    private float amplitude;

    private bool moving;

    // Use this for initialization
    void Start () {
        setUp();
        // StartCoroutine(startSin());
    }

    void setUp() {
        sinTime = 0f;
        cumulitiveTime = 0f;
        frequency = 1;
        amplitude = range / 10;
        Arrow = control.arrow;
        // direction = 1;
        moving = false;
    }

    void pingpong() {
        if(moving) {
            Arrow.transform.RotateAround(
           this.transform.position,
           Vector3.back,
           sinMovement()
           );
        }
    }

    IEnumerator startSin() {
        yield return new WaitForSeconds(0.5f);
        moving = true;
    }

    public void startMovement() {
        moving = true;
    }

    float sinMovement() {

        cumulitiveTime += Time.deltaTime;

        // if the timer hasn't been set
        // set it to the current time.
        if(sinTime == 0) {
            sinTime = cumulitiveTime + frequency * 0.75f;
        }

        // perform the sinoidal movement
        float timeDif = (sinTime - cumulitiveTime);
        float theta = timeDif / (frequency / 2);
        float distance = amplitude * Mathf.Sin(theta);
        return distance;
    }

    // casts ray, returns collider struck
    public Collider2D fireRay() {
        ray = Physics2D.Raycast(transform.position, Arrow.transform.position, Mathf.Infinity,logLayerMask);
        return ray.collider;
    }
	
	// Update is called once per frame
	void Update () {
        currentSpeed = difficultyMult * GameController.Difficulty;
        pingpong();
    }
}
