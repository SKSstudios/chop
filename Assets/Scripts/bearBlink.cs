﻿using UnityEngine;
using System.Collections;

public class bearBlink : MonoBehaviour {

    public float lowerTimeLimit, upperTimeLimit;
    public bool isBlinking;

	// Use this for initialization
	void Start () {
        StartCoroutine(blinkRoutine());
	}

    public IEnumerator blinkRoutine() {
        float timeWait;

        while(true) {
            timeWait = Random.Range(lowerTimeLimit, upperTimeLimit);
            yield return new WaitForSeconds(timeWait);

            if (isBlinking) {

                // blinks once
                GetComponent<sprite_swapper>().setSprite(1);
                yield return new WaitForSeconds(0.07f);
                GetComponent<sprite_swapper>().setSprite(0);

                // 50% chance to blink immediately again
                if (Random.Range(0, 2) == 0) {
                    yield return new WaitForSeconds(0.5f);
                    GetComponent<sprite_swapper>().setSprite(1);
                    yield return new WaitForSeconds(0.07f);
                    GetComponent<sprite_swapper>().setSprite(0);
                }
            }
        }
        
    }

    public void manualOpenEyes() {
        GetComponent<sprite_swapper>().setSprite(0);
    }

    public void manualSquint() {
        GetComponent<sprite_swapper>().setSprite(2);
    }

    public void manualCloseEyes() {
        GetComponent<sprite_swapper>().setSprite(1);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
