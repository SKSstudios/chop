﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public GameObject TESTOBJECT;

    public Director director;
    public static RaycastHit2D currentTouch;
    public GameObject rayCaster, arrow;
    public Camera cam;
    public SplinterBehavior splinterBehavior;
    public LogSpawner logSpawner;

    public Material _defaultMaterial;

    public GameObject stump;

    // if the user is playing
    // game is not over and is not
    // in a menu
    public static bool playing;

    // is the game currently "chopping"
    public static bool chopping;

    // arbitrary numerical representation of difficulty
    // game will get more difficult as this increases.
    public static float Difficulty;

    // frequency that the arrow makes a full partial rotation
    // around the track.
    public static float frequency;
    public static float score;
    public static int lives;

    public float lineAngleRange, lineOffsetRange, lineWidth, lineNum;

    // chops about to occur. Set from pointControl and chops
    // are activated from callBack.
    List<List<Vector3>> chopPoints;

    // currently active log
    public GameObject activeLog;

    // Use this for initialization
    void Start () {
        currentTouch = Physics2D.Raycast(Vector2.zero, Vector2.zero);
        setupScene();
	}

    void setupScene() {
        Difficulty = 1;
        director = GetComponent<Director>();
    }

    public void startPlaying() {
        playing = true;
    }

    #region depreciated section
    /////
    ///// POTENTIALLY DEPRECIATED
    /////

    //public void startPlay() {
    //    rayCaster.GetComponent<rayCaster>().startMovement();
    //}

    //void attemptSlice() {
    //    List<GameObject> splitMembers = new List<GameObject>();
    //    if (Input.GetKeyDown("space") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) {

    //        // fires ray from rayCaster sets chopped to
    //        // the collider struck (if any)
    //        Collider2D chopped = rayCaster.GetComponent<rayCaster>().fireRay();

    //        // if the chop performed was in the chop bounds
    //        if (chopped != null && hitChop(chopped)) {

    //            // chop the log
    //            // chopped.transform.parent.gameObject.GetComponent<logBehavior>().split();
    //        }
    //    }
    //}

    //// will return true if the chop
    //// was in bounds, and false if
    //// the chop missed the bounds.
    //bool hitChop(Collider2D chopResult) {
    //    return true;
    //}

    /////
    ///// END DEPRECIATION
    /////
    #endregion

    // sets the log to be chopped
    // called from logSpawn.
    public void setLog(GameObject log) {
        activeLog = log;
    }

    // places a dot on the current touch location
    // if the game is playing.
    void placeDot() {

        // if no touches, return.
        if(Input.touchCount == 0) {
            return;
        }

        // if the player is touching the background,
        // the chopcontrol is not currently placing a dot, 
        // and the game is playing, place a new dot at the
        // current touch location.
        if(currentTouch.transform != null &&
            Input.GetTouch(0).phase == TouchPhase.Began &&
            currentTouch.transform.gameObject.tag.Equals("BackGround") && 
            !director.chopControl.isPlacingDot && 
            playing) {

            // creates the chop
            director.chopControl.createSlice(
               director.cam.ScreenToWorldPoint((Vector3)Input.GetTouch(0).position));
        }
    }

    // will validate chop locations AND set the 
    // chop points to be cut.
    public void validateChops(List<List<Vector3>> chopPoints) {

       
        // retrieves pairs of points from the given current chops
        foreach (List<Vector3> vectorTuple in chopPoints) {

            bool singleSuccess;

            // points to be tested
            Vector2[] pointsToVerify = getFillPoints(vectorTuple);

            // DEBUG_SPAWN_GEN_POINTS(pointsToVerify);

            // disabled for debugging
            // disabled for debugging
            // disabled for debugging
            singleSuccess = activeLog.GetComponent<LogCapsule>().logObject.GetComponent<LogLineStore>().isInTolerance(pointsToVerify);
            Debug.Log("Line verified: " + singleSuccess);
        }

        bool totalSuccess = activeLog.GetComponent<LogCapsule>().logObject.GetComponent<LogLineStore>().isSuccessfulTurn();



        // TODO- ONLY INCLUDE SUCCESSFUL CHOPS.
        // UNSUCCESSFUL CHOPS WILL -NOT- CUT LOG.
        this.chopPoints = chopPoints;

    }

    // spawns points that will be polled between the two points placed by the user
    public void DEBUG_SPAWN_GEN_POINTS(Vector2[] locations) {
        foreach(Vector2 location in locations) {
            Instantiate(TESTOBJECT, location, Quaternion.identity);
        }
    }

    // called from checkChops
    // returns an array of vector2's extruded from the parent and vector
    // locations, bounded by their positions.
    // 
    public Vector2[] getFillPoints(List<Vector3> vectorTuple) {

        // little bitch thinking it can typecast as int
        float extrudeSize = 10;

        Vector2[] extrudedPoints = new Vector2[(int)extrudeSize];

        // string debugStr = "";
        // gets points between the vector2's
        for (int i = 0; i < extrudeSize; i++) {
            // debugStr += ("t : " + ((i + 1) / extrudeSize) + " on " + vectorTuple[0] + " and " + vectorTuple[1] + "\n");
            extrudedPoints[i] = Vector2.Lerp(vectorTuple[0], vectorTuple[1], i / extrudeSize);
        }

        //Debug.Log(debugStr);

        return extrudedPoints;
    }

    // callBack from routineObject when the points
    // have finished sliding
    public void coRoutineCallbackInitSlicing() {
        chopping = false;
        initSlicing(activeLog, chopPoints);
    }

    // performs the slicing on the log. Called if chops are validated.
    // moves splinters to the proper parent object.
    private void initSlicing(GameObject log, List<List<Vector3>> chopPoints) {

        chopping = true;

        // Slicer.slice(log, chopPoints[0][0], chopPoints[0][1]);
        int logmask = 1 << 8;

        // list with single log to be fed into slicer
        List<GameObject> singleLog = new List<GameObject>();
        singleLog.Add(log.GetComponent<LogCapsule>().logObject);

        // splits the log and it's splinters
        ///////////// not getting all gameObjects
        List<GameObject> splinters =
            Slicer.startRecursiveNonDestructiveSlice(
                singleLog,
                chopPoints,
                logmask
                );

        // retrieves extra splinters
        List<GameObject> splintersExtra = log.GetComponent<LogCapsule>().cleanUpSplinters();

        // appends straggler splinters to splinters list
        splinters.AddRange(splintersExtra);

        foreach (GameObject splinter in splinters) {
            splinter.transform.SetParent(director.splinterParent);

            // if the passed is an entire log
            if(splinter.GetComponent<MeshRenderer>() != null) {
                splinter.GetComponent<MeshRenderer>().material = _defaultMaterial;
            }

            splinter.tag = "fragment";
            splinter.layer = stump.layer;
        }
        splinterBehavior.explodeSplinters(splinters);
        // splinterBehavior.freezeSplinters(splinters);
    }


    void setCurrentTouch() {
        // if there is a touch, set the current touch to it
        if(Input.touchCount > 0) {
            currentTouch = Physics2D.Raycast(
                director.cam.ScreenToWorldPoint(Input.GetTouch(0).position), 
                Vector2.zero);
        }
    }

    public void DEBUG_SPAWN_LINED_LOG() {
        logSpawner.genLogWithGuides((int)lineNum, lineWidth);
    }


	
	// Update is called once per frame
	void Update () {
        setCurrentTouch();
        placeDot();
        // attemptSlice();
	}
}
