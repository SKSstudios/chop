﻿using UnityEngine;
using System.Collections;

public class camera_target : MonoBehaviour {

    public Vector3 currentTarget;
    public GameObject bearTarget, gameTarget;
    public Camera camera;
    public float moveTime;

    private bool isMoving;
    private float currentLerp;

	// Use this for initialization
	void Start () {
        // setTarget(bearTarget.transform.position);
        firstPos(bearTarget.transform.position);
	}

    public void firstPos(Vector3 firstTarget) {
        camera.transform.position = firstTarget;
    }

    // sets the camera's new target
    public void setTarget(Vector3 newTarget) {
        currentLerp = 0f;
        isMoving = true;
        currentTarget = newTarget;
    }

    void moveToTarget() {

        // if the camera has reached it's destination
        // stop trying to track an object
        if(transform.position == currentTarget) {
            isMoving = false;
            return;
        }
        currentLerp += Time.deltaTime;
        float lerpStep = currentLerp / moveTime;

        // performs smoothstep towards target vector3
        camera.transform.position = new Vector3(
            Mathf.SmoothStep(camera.transform.position.x, currentTarget.x, lerpStep),
            Mathf.SmoothStep(camera.transform.position.y, currentTarget.y, lerpStep),
            camera.transform.position.z
            );
    }
	
	// Update is called once per frame
	void Update () {
	    if(isMoving) {
            moveToTarget();
        }
	}
}
