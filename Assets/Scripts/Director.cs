﻿using UnityEngine;
using System.Collections;

///
/// Directs camera movement, as well as any
/// script connections necessary. Neural hub
/// of game.
///

public class Director : MonoBehaviour {

    // scripts
    public screen_shake shake;
    public screen_shake[] maskShake;
    public Zoom zoom;
    public ResourceMaster resourceMaster;
    public ChopPointControl chopControl;
    public LogSpawner logSpawn;
    public Camera cam;
    public CounterDisplay countDisplay;
    public coRoutineObject coRoutineMaster;
    public LogLineMaster logLineMaster;
    public LogLineDrawer logLineDrawer;
    public bearAnimController bearAnimControl;

    public GameObject chopLineRenderer;

    public Transform splinterParent;

    // current camera target
    public camera_target target;

    // initial zoom on game start
    public float initialZoom;

    // first pos when game starts
    public Vector3 initialPos;

    // scale of new log and dot objects
    public Vector3 logScale, dotScale, guideScale;

    // how many objects to spool
    public int logCount, dotCount;

    // speed of the dot tracking the touch
    public float dotTrackSpeed, arrowGuideSpeed;

    // time for the parent dot to reach the vector dot when chopping
    public float chopSpeed, chopDelay;

    // how much the screen shakes when a log lands
    public float logLandShakeAmtX, logLandShakeAmtY, logLandShakeTime;
    public float chopShakeAmt, chopShakeTime;
    public float explosionShakeAmt, explosionShakeTime;


    // force with which the log splits
    public float chopExplosionForce, explosionRadius, explosionLift;

    public Transform explosionOrigin;

    // Use this for initialization
    void Start () {
        setupScene();
	}

    // sets up scripts in the scene
    // as well as camera positions
    public void setupScene() {
        shake.Init();
        initPositions();
        setupResourceMaster();
        setupChopControl();
        setupLogLineControl();
        setupLogSpawner();
        setupCountDisplay();

        startSleepingAnims();
        // countDisplay.reset(3);

        // the transform that splintered logs will be transferred to
        Splitter.splinterParent = splinterParent;
    }

    // starts bear idle
    private void startSleepingAnims() {
        bearAnimControl.init();
    }

    //public void wakeUpSequenceRun() {
    //    // bearAnimControl.runAnims();
    //}

    private void setupCountDisplay() {
        countDisplay.setup();
        countDisplay.director = this;
    }

    private void setupChopControl() {
        chopControl = new ChopPointControl();
        chopControl.director = this;
        chopControl.controller = GetComponent<GameController>();
        chopControl.setup();

        ///
        /// DEBUG
        /// DEBUG
        /// DEBUG
        ///
        chopControl.setNumChops(3);
    }

    private void setupResourceMaster() {
        resourceMaster = new ResourceMaster();
        resourceMaster.director = this;
        resourceMaster.setup();
        resourceMaster.spoolAssets();
    }

    private void setupLogSpawner() {
        logSpawn.director = this;
        logSpawn.resources = resourceMaster;
        logSpawn.control = GetComponent<GameController>();
        logSpawn.setup();
        // logSpawn.startLogSpawnDelay(2f);
    }

    private void setupLogLineControl() {
        logLineDrawer = new LogLineDrawer();
        logLineMaster = new LogLineMaster();

        logLineDrawer.director = this;
        logLineMaster.director = this;

        logLineMaster.drawer = logLineDrawer;
        logLineDrawer.lineMaster = logLineMaster;

        LogLineStore.lineMaster = logLineMaster;
        LogLineMaster.chopLineRenderer = chopLineRenderer;

        logLineMaster.logSpawner = logSpawn;

        logLineMaster.setup();
    }

    // sets initial positions
    public void initPositions() {
        zoom.firstZoom(initialZoom);
        target.firstPos(target.bearTarget.transform.position);
    }

    // called when wake-up animations have finished
    public void zoomOutCallback() {
        coRoutineMaster.DEBUG_INTRO_CALLBACKCOMPLETE();
    }
    // Now called from callback from coroutineObject.
    public void startButtonZoom() {
        int targetZoom = 320;
        Vector3 cameraTarget = target.gameTarget.transform.position;

        zoom.startZoom(targetZoom);
        target.setTarget(cameraTarget);
    }

    // shakes the screen when a log lands
    public void shakeLand() {
        shake.ScreenShake(logLandShakeAmtX,logLandShakeAmtY,logLandShakeTime);
        //foreach(screen_shake)
        //maskShake.ScreenShake(logLandShakeAmtX, logLandShakeAmtY, logLandShakeTime);
    }

    public void shakeChop() {
        shake.ScreenShake(chopShakeAmt, chopShakeAmt, chopShakeTime);
        // maskShake.ScreenShake(chopShakeAmt, chopShakeAmt, chopShakeTime);
    }

    public void shakeExplode() {
        shake.ScreenShake(explosionShakeAmt, explosionShakeAmt, explosionShakeTime);
        //maskShake.ScreenShake(chopShakeAmt, chopShakeAmt, chopShakeTime);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
