﻿using UnityEngine;
using System.Collections;

public class Zoom : MonoBehaviour {

    public Camera camera;
    public float sizeTarget;
    public float zoomTime;

    private bool isMoving;
    private float currentLerp;

    // Use this for initialization
    void Start () {
        // firstZoom();
	}

    public void firstZoom(float initialZoom) {
        camera.orthographicSize = initialZoom;
    }

    void zoom() {
        if(camera.orthographicSize == sizeTarget) {
            isMoving = false;
            return;
        }

        currentLerp += Time.deltaTime;
        float lerpstep = currentLerp / zoomTime;

        camera.orthographicSize = Mathf.SmoothStep(
            camera.orthographicSize,
            sizeTarget,
            lerpstep);
    }
    
    // starts a zoom to the given size
    public void startZoom(int newTargetSize) {
        sizeTarget = newTargetSize;
        isMoving = true;
    }
	
	// Update is called once per frame
	void Update () {
	    if(isMoving) {
            zoom();
        }
	}
}
