﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourceMaster : MonoBehaviour {

    public Director director;

    public ObjectBuffer parentDotBuffer, vectorDotBuffer, arrowGuideBuffer;
    public ObjectBuffer logBuffer;

    // directories of the prefabs
    private string parentDotBufferDir = "Prefabs/dots/dot_chop_parent";
    private string vectorDotBufferDir = "Prefabs/dots/dot_chop_vector";
    // private string arrowGuideBufferDir = "Prefabs/dots/arrow_guide";
    private string logBufferDir       = "Prefabs/logs/logcap/Log_Capsule";

    private string bufferDir = "Prefabs/components/object_buffer";


	// Use this for initialization
	void Start () {

	}

    public void setup() {
        parentDotBuffer = new ObjectBuffer();
        vectorDotBuffer = new ObjectBuffer();
        // arrowGuideBuffer = new ObjectBuffer();
        logBuffer       = new ObjectBuffer();
    }

    public void spoolAssets() {

        // spools dot parents
        parentDotBuffer.setup(
            Resources.Load<GameObject>(parentDotBufferDir),
            director.dotScale,
            director.dotCount
            );

        // spools dot vectors
        vectorDotBuffer.setup(
            Resources.Load<GameObject>(vectorDotBufferDir),
            director.dotScale,
            director.dotCount
            );

        logBuffer.setup(Resources.Load<GameObject>(logBufferDir),
            director.logScale,
            director.logCount
            );
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
