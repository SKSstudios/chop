﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectBuffer : MonoBehaviour {


    static int defaultSize = 10;

    // size at startup. Not an active count.
    int size;
    Vector3 scale;

    // used for modifying the sprite of the objects
    // on the spool
    public Sprite objectSprite;

    public List<GameObject> buffer;
    public GameObject prefab;

    // Use this for initialization
    void Start() {

    }

    public void changeSprite(Sprite newSprite) {
        objectSprite = newSprite;
        foreach (GameObject element in buffer) {
            element.GetComponent<SpriteRenderer>().sprite = objectSprite;
        }
    }

    public void setup(GameObject prefab, Vector3 scale, int size) {
        this.size = size;
        this.scale = scale;

        // if no size set, size is default size
        if (size == 0)
            size = defaultSize;

        buffer = new List<GameObject>(size);
        this.prefab = prefab;

        for (int i = 0; i < size; i++) {
            buffer.Add(instantiate(prefab, scale));
        }
    }

    // generates a new, deactivated gameObject at (0,0,0)
    private GameObject instantiate(GameObject prefab, Vector3 scale) {
        GameObject retObj = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
        retObj.SetActive(false);

        // if scale was provided
        retObj.GetComponent<Transform>().localScale = scale;

        // if the object has a spriteRenderer and the spool has
        // a sprite ready, apply that to the object
        if (retObj.GetComponent<SpriteRenderer>() != null && objectSprite != null) {
            retObj.GetComponent<SpriteRenderer>().sprite = objectSprite;
        }

        buffer.Add(retObj);

        return retObj;
    }

    public void positionObjects(Vector3 position) {
        for (int i = 0; i < buffer.Count; i++) {
            buffer[i].GetComponent<Transform>().position = position;
        }
    }

    // sets the scale of all objects in the buffer
    public void scaleObjects(Vector3 scale) {
        for (int i = 0; i < buffer.Count; i++) {
            buffer[i].GetComponent<Transform>().localScale = scale;
        }
    }

    // sets the size of the arrayList (only increase)
    public void setSize(int size) {
        this.size = size;
    }

    public GameObject getObject(Vector3 scale, Vector3 position) {

        // looks for an inactive gameObject
        for (int i = 0; i < buffer.Count; i++) {
            if (!buffer[i].activeInHierarchy) {
                buffer[i].transform.localScale = scale;
                buffer[i].transform.position = position;
                buffer[i].GetComponent<Transform>().position = position;
                buffer[i].SetActive(true);
                // buffer[i].GetComponent<Transform>().localScale = scale;
                // buffer[i].GetComponent<Transform>().position = position;
                return buffer[i];
            }
        }

        // if none found, instantiate a new one + 1 buffer object
        instantiate(prefab, scale);
        GameObject newWall = instantiate(prefab, scale);
        newWall.GetComponent<Transform>().position = position;
        return newWall;
    }

    public void removeObject(GameObject removed) {
        removed.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }
}
