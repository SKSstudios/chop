﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WheelRotation : MonoBehaviour {

    public float rotationSpeed;
    Vector2 firstTouchPos, currentTouchPos;
    

	// Use this for initialization
	void Start () {
	
	}

    void rotateToPoint() {
        
        // if the object is being touched right now
        if (GameController.currentTouch.collider == GetComponent<Collider2D>()) {

            // if no touches, return.
            if(Input.touchCount == 0) {
                return;
            }

            // if the touch has moved or is stationary, start rotation
            if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary) {
                Transform trans = GetComponent<Transform>();
                GetComponent<Transform>().Rotate(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, trans.position.z);
            }
        }
    }

    // updates touch positions
    void touchUpdate() {

        // if there is a touch
        if(Input.touchCount > 0) {

            // if the collider hit is this object's collider
            if (GameController.currentTouch.collider == GetComponent<Collider2D>()) {

                // set the new touch position to the current touch's point
                // will create delta movement if the touch is not new
                currentTouchPos = GameController.currentTouch.point;

                // if this is the first touch, set the last to the new,
                // as the finger has not moved.
                if (Input.GetTouch(0).phase == TouchPhase.Began) {
                    firstTouchPos = currentTouchPos;
                }
            }

            // else reset both to zero
            else {
                currentTouchPos = Vector2.zero;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        touchUpdate();
        rotateToPoint();
	}
}
