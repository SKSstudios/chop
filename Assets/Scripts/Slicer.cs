﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Slicer : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

    // returns a list of sliced objects
    // from a single target.
    public static List<GameObject> slice(GameObject sliced, Vector3 point1, Vector3 point2) {
        
        List<SpriteSlicer2DSliceInfo> slicedRet = new List<SpriteSlicer2DSliceInfo>();
        List<GameObject> returnedObjects = new List<GameObject>();
        SpriteSlicer2D.SliceSprite(point1, point2, sliced, true, ref slicedRet);
        if(slicedRet.Count == 0) {
            return null;
        }
        returnedObjects = slicedRet[0].ChildObjects;
        return returnedObjects;
    }

    public static List<GameObject> startRecursiveNonDestructiveSlice(List<GameObject> sliced, List<List<Vector3>> points, int layerMask) {
        return nonDestructiveSlice(sliced, 0, points, layerMask);
    }

    // slices a gameObject multiple times, and returns the slices.
    // points provides a list of pairs of vector3 slice locations.
    public static List<GameObject> nonDestructiveSlice(List<GameObject> sliced, int currentSlice, List<List<Vector3>> points, int layerMask) {

        // end recursion
        if (currentSlice == points.Count) {
            return sliced;
        }

        // list of gameObjects going to be returned
        List<GameObject> returnedObjects = new List<GameObject>();

        // list of slices going to be returned
        List<SpriteSlicer2DSliceInfo> slicedRet = new List<SpriteSlicer2DSliceInfo>();

        // if this is the first slice, peserve the object (for processing- re-spooling)
        if (currentSlice == 0) {

            // slices the log, maintains the log object
            SpriteSlicer2D.SliceSprite(
                points[currentSlice][1], 
                points[currentSlice][0], 
                sliced[0], 
                false, 
                ref slicedRet
                );

            if(slicedRet.Count == 0) {
                Debug.Log("no children returned");
                return sliced;
            }

            // feeds splinters into the alg recursively
            return nonDestructiveSlice(
                   bundleSplinters(slicedRet),
                   currentSlice + 1,
                   points,
                   layerMask
                   );

        }

        // if this is a splinter, enter the recursive splinter call
        else {

            SpriteSlicer2D.SliceAllSprites(
                    points[currentSlice][1],
                    points[currentSlice][0],
                    true,
                    ref slicedRet,
                    layerMask);

            return  nonDestructiveSlice(
                    bundleSplinters(slicedRet),
                    currentSlice + 1,
                    points,
                    layerMask
                    );
            }

        }

    // bundles the children from a list of spriteslicer2dinfo into a list of gameObjects
    public static List<GameObject> bundleSplinters(List<SpriteSlicer2DSliceInfo> slicedRet){

        // gathers all splinters from all children
        List<GameObject> CompiledChildObjects = new List<GameObject>();
        foreach (SpriteSlicer2DSliceInfo slices in slicedRet) {
            foreach (GameObject splinter in slices.ChildObjects) {
                CompiledChildObjects.Add(splinter);
            }
        }
        return CompiledChildObjects;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
