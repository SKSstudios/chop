﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class logBehavior : MonoBehaviour {


    // set when the log spawns
    public renderCamAdjust camAdjust;
    public GameController control;
    public Director director;
    public LogSpawner spawn;
    public LogAppear appear;
    public ChopLineRenderSpawner chopLineSpawner;

    // force that the log splits with
    public float splitForce;

    public bool hasShaken;

	// Use this for initialization
	void Start () {
        splitForce = 7000;
        appear = GetComponent<LogAppear>();
	}

    public void reset() {
        camAdjust.setDimensions();
        freeze();
        hasShaken = false;
    }

    // disables this log, sending it back to the spool
    public void disabled() {
        appear.isFinished = false;
        appear.dissappear();
        GetComponent<GameObject>().SetActive(false);
    }

    public void freeze() {
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void unfreeze() {
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
    }

    public bool split() {
        
        return true;
    }

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "Stump" && !hasShaken) {
            director.shakeLand();
            hasShaken = true;
        }
    }

    // Update is called once per frame
    void Update () {

        // if the log is frozen but also done expanding, unfreeze it.
	    if(GetComponent<Rigidbody2D>().constraints.Equals(RigidbodyConstraints2D.FreezeAll) 
            && transform.parent.gameObject.GetComponent<LogAppear>().isFinished) {
            unfreeze();
        }
	}
}
