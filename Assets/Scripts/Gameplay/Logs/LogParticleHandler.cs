﻿using UnityEngine;
using System.Collections;

public class LogParticleHandler : MonoBehaviour {

    public int numParticlesSpawn;
    public ParticleSystem poof1, poof2;

	// Use this for initialization
	void Start () {
        // triggerPoofs();
	}

    public void triggerPoofs() {
        poof1.Emit(numParticlesSpawn);
        poof2.Emit(numParticlesSpawn);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
