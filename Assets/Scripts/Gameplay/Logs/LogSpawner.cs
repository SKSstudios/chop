﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LogSpawner : MonoBehaviour {

    public Image statusBoxDEBUG;

    public Director director;
    public ResourceMaster resources;
    public GameController control;
    public LogParticleHandler particleHandler;
    public LogLineMaster logLineMaster;
    public Camera mainCam;

    // starting size of newly spawned logs
    public float startSize;

    // max tilt of a newly spawned log
    public float maxTilt;

    // time for new log to appear
    public float fadeTime;

	// Use this for initialization
	void Start () {
        
	}

    // sets up each log, attatching their proper scripts
    // runs after resourceMaster is setup.
    public void setup() {
        foreach(GameObject log in resources.logBuffer.buffer) {
            logBehavior newLogBehavior = log.GetComponent<LogCapsule>().logObjectBehavior;
           
            newLogBehavior.spawn = this;
            newLogBehavior.control = control;
            newLogBehavior.director = director;

            log.GetComponent<LogAppear>().director = director;
            log.GetComponent<LogAppear>().dissappear();
        }

        logLineMaster = director.logLineMaster;
    }

    // spawns a log and draws lines on it
    public void genLogWithGuides(int numGuides, float width) {
        logLineMaster.drawLinesOnLog(spawnLog(), numGuides, width);
    }

    // starts the spawning of a log with a given delay
    public void startLogSpawnDelay(float delay) {
        StartCoroutine(newLog(delay));
    }

    // waits for delay seconds then spawns logs
    public IEnumerator newLog(float delay) {
        yield return new WaitForSeconds(delay);
        spawnLog();
    }

    // spawns a log from the spool
    public GameObject spawnLog() {

        // gets a new log from the spool with scale set from the director
        // and location set to the spawner's location
        GameObject newLog = resources.logBuffer.getObject(director.logScale, GetComponent<Transform>().position);
        logBehavior newLogBehavior = newLog.GetComponent<LogCapsule>().logObjectBehavior;

        // set's the new log's scripts
        newLogBehavior.spawn = this;
        newLogBehavior.control = control;
        newLogBehavior.director = director;
        // newLogBehavior.appear = newLog.GetComponent<LogAppear>();

        // freezes the log, resets everything else
        newLogBehavior.reset();

        // makes the log start to appear
        newLog.GetComponent<LogAppear>().appear(fadeTime, startSize);

        // makes the poof effect trigger
        particleHandler.triggerPoofs();

        // gives the log a slight random rotation
        // randomRot(newLog);

        // sets the gameController's current log to the active one
        control.activeLog = newLog;

        // initializes log line behavior
        newLogBehavior.chopLineSpawner.init(ChopLineRenderSpawner.maxNumLines);

        newLogBehavior.gameObject.GetComponent<LogLineStore>().control = control;
        newLogBehavior.gameObject.GetComponent<LogLineStore>().statusBoxDEBUG = statusBoxDEBUG;

        // Debug.Log(newLog.GetComponent<LogCapsule>().drawRect.mainCam);
        newLog.GetComponent<LogCapsule>().drawRect.mainCam = mainCam;
        newLog.GetComponent<LogCapsule>().matAdjust.setDimensions();

        control.setLog(newLog);

        return newLog;
    }

    // randomly rotates the log slightly
    public void randomRot(GameObject log) {
        log.transform.rotation = Quaternion.identity;
        float numDegs = Random.Range(-maxTilt,maxTilt);
        log.transform.Rotate(0f, 0f, numDegs);
    }
	
	// Update is called once per frame
	void Update () {

	}
}
