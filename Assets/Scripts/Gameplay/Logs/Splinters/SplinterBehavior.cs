﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SplinterBehavior : MonoBehaviour {

    // material with negative friction, causing the log to fly apart.
    PhysicsMaterial2D negativeFricMat;

    public Director director;

	// Use this for initialization
	void Start () {
	
	}

    public static void AddExplosionForce(Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius) {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        body.AddForce(dir.normalized * explosionForce * wearoff);
    }

    public static void AddExplosionForce(Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier) {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        Vector3 baseForce = dir.normalized * explosionForce * wearoff;
        body.AddForce(baseForce);

        float upliftWearoff = 1 - upliftModifier / explosionRadius;
        Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
        body.AddForce(upliftForce);
    }

    // starts routine that waits a frame, then applies explosion physics to the log.
    // shakes screen.
    public void explodeSplinters(List<GameObject> splinters) {
        StartCoroutine(explosionFrameWait(splinters));
    }

    // explodes the gameObject after fps is > 30
    public IEnumerator explosionFrameWait(List<GameObject> splinters) {
        bool frameReturn = false;
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        while (frameReturn == false) {
            yield return new WaitForEndOfFrame();
            if(Time.deltaTime < 0.20f) {
                frameReturn = true;
            }
        }
        foreach (GameObject splinter in splinters) {

            if (splinter == null)
                continue;

            AddExplosionForce(
                splinter.GetComponent<Rigidbody2D>(),
                director.chopExplosionForce,
                director.explosionOrigin.position,
                director.explosionRadius,
                director.explosionLift);

            // splinter.GetComponent<Collider2D>().enabled = false;
        }
        director.shakeExplode();
    }

    // applies the material to all splinters passed
    public void applyFricMat(List<GameObject> splinters) {
        foreach(GameObject splinter in splinters) {
            splinter.GetComponent<PolygonCollider2D>().sharedMaterial = negativeFricMat;
        }
    }

    public void freezeSplinters(List<GameObject> splinters) {
        foreach (GameObject splinter in splinters) {
            splinter.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
