﻿using UnityEngine;
using System.Collections;

public class LogAppear : MonoBehaviour {

    public Director director;
    public GameObject logObject;
    public Camera logCam;
    public float initCamSize;

    // if this log has completed it's animation yet
    public bool isFinished;
 
    public float lerpTime, currentLerp;

	// Use this for initialization
	void Start () {
        // GetComponent<SpriteRenderer>().enabled = false;
	}

    // makes the log expand from the given scale in the given time
    public void appear(float time, float scale) {

        // the log is just appearing so it is not finished
        isFinished = false;

        // sets the time for the lerp
        lerpTime = time;

        // makes the transform's starting scale smaller
        Transform trans = GetComponent<Transform>();
        trans.localScale *= scale;

        initCamSize = logCam.orthographicSize;
        logCam.orthographicSize *= scale;

        // make the log visible
        logObject.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void dissappear() {
        logObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    // run every frame, makes the log expand
    void expand() {

        // if the size is already correct, return
        if (transform.localScale.x >= director.logScale.x) {
            isFinished = true;
            return;
        }

        // lerps the scale
        currentLerp += Time.deltaTime;
        float lerpMod = currentLerp / lerpTime;
        Vector3 newScale = new Vector3(
            Mathfx.Coserp(transform.localScale.x, director.logScale.x, lerpMod),
            Mathfx.Coserp(transform.localScale.y, director.logScale.y, lerpMod),
            Mathfx.Coserp(transform.localScale.z, director.logScale.z, lerpMod)
            );
        float newOrtho = Mathfx.Coserp(logCam.orthographicSize, initCamSize, lerpMod);

        // applies the new scale
        transform.localScale = newScale;
        logCam.orthographicSize = newOrtho;
    }
	
	// Update is called once per frame
	void Update () {
        expand();
	}
}
