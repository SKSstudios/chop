﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///
/// Stores chop line information
///

public class LogLineStore : MonoBehaviour {

    public GameObject DEBUGPOINT;
    public GameObject DEBUCPOINTCONFIRMED;
    public Image statusBoxDEBUG;
    public Transform DEBUGTRANSFORM;

    public Transform lineTrans;
    public LogCapsule parentCap;
    public GameObject chopLineRenderer;
    public static LogLineMaster lineMaster;
    public Transform chopLineSpawnerTransform;
    public GameController control;

    private float width;
    private float[] slopes;
    private float[] hmodX, hmodY;
    private float[] hmodWorldX, hmodWorldY;

    public bool hasSetSpacing;
    // if all true, log has been successfully cut
    public bool[] confirmedLines;

	// Use this for initialization
	void Start () {
        parentCap = transform.parent.gameObject.GetComponent<LogCapsule>();
    }

    public void setLines(float[] slopes, float[] hmodX, float[] hmodY, float[] hmodWorldX, float[] hmodWorldY, float width) {
        this.slopes = slopes;
        this.hmodX = hmodX;
        this.hmodY = hmodY;
        this.hmodWorldX = hmodWorldX;
        this.hmodWorldY = hmodWorldY;
        this.width = width;

        confirmedLines = new bool[hmodX.Length];
        for(int i = 0; i < hmodX.Length; i++) {
            confirmedLines[i] = false;
        }

        hasSetSpacing = false;
        
    }


    ///
    /// requires tesing
    /// requires testing
    ///
    public bool isInTolerance(Vector2[] testingPoints) {

        Vector2 worldspace = this.transform.position;

        float planeWidth = parentCap.matAdjust.meshWidth;
        float planeHeight = parentCap.matAdjust.meshHeight;
        float ratio = planeWidth / planeHeight;

        for (int i = 0; i < testingPoints.Length; i++) {

            // gets list of X test points
            float[] Xs = new float[testingPoints.Length];
            for (int j = 0; j < testingPoints.Length; j++) {
                Xs[j] = testingPoints[j].x;
            }

            for (int p = 0; p < slopes.Length; p++) {

                // if this line has already been confirmed, do not test it.
                if (confirmedLines[p])
                    continue;

                Vector2[] genPoints = getDerivedLines(slopes[p], hmodX[p], hmodY[p], Xs, worldspace);
                // Vector2[] genPointsTESTPARENTLOG = getDerivedLinesLOCALDEBUG(slopes[p], hmodX[p], hmodY[p], Xs);
                
                // DEBUG YALL
                for(int y = 0; y < genPoints.Length; y++) {

                    // calculates the distance between the generated and supplied
                    // points. If it falls within the range of the line, this chop
                    // has been confirmed to be valid.
                    float distance = Vector2.Distance(genPoints[i], testingPoints[i]);
                    // Debug.Log("DISTANCE: " + distance);

                    // if the distance is greater than the width, break,
                    // and continue on to test the next line.
                    if (distance > width / 2) {
                        break;
                    } else {
                        // Instantiate(DEBUGPOINT, genPoints[y], Quaternion.identity);
                    }

                    // the line that is being checked will be set to true if it was successful
                    if(y == genPoints.Length - 1) {
                        confirmedLines[p] = true;
                        Debug.Log("Log pos verified");
                        return true;
                    }
                  
                }
            }
        }
        // Time.timeScale = 0f;
        return false;
    }


    public bool isSuccessfulTurn() {
        foreach (bool confirmed in confirmedLines) {
            if (!confirmed) {
                statusBoxDEBUG.color = Color.red;
                return false;
            }
        }
        statusBoxDEBUG.color = Color.green;
        return true;
    }

    // retreives a list of vector2 points derived from xvalues on the
    // input lines.
    public Vector2[] getDerivedLinesLOCALDEBUG(float slope, float Yoffset, float Xoffset, float[] Xs) {

        Vector2[] checkPoints = new Vector2[Xs.Length];

        float[] Ys = new float[Xs.Length];

        for (int i = 0; i < Xs.Length; i++) {
            Ys[i] = (Xs[i] - Xoffset) * (slope) + Yoffset;
            // Ys[i] = (Xs[i]) * slope;
            checkPoints[i] = new Vector2(Xs[i], Ys[i]);
        }




        return checkPoints;

        // LINERENDER_DEBUG_DRAW_LINES(point1, point2);
    }

    // retreives a list of vector2 points derived from xvalues on the
    // input lines.
    public Vector2[] getDerivedLines(float slope, float Yoffset, float Xoffset, float[] Xs, Vector2 worldspace) {

        Vector2[] checkPoints = new Vector2[Xs.Length];

        float[] Ys = new float[Xs.Length];

        for(int i = 0; i < Xs.Length; i++) {
            Ys[i] = (Xs[i] - worldspace.x - Xoffset) * (slope) + Yoffset;
            // Ys[i] = (Xs[i]) * slope;
            checkPoints[i] = new Vector2(Xs[i], Ys[i] + worldspace.y);
        }

        
        

        return checkPoints;

        // LINERENDER_DEBUG_DRAW_LINES(point1, point2);
    }

    public void DEUBUG_DRAW() {
        for(int i = 0; i < slopes.Length; i++) {
            float width = parentCap.logObject.GetComponent<SpriteRenderer>().bounds.extents.x * parentCap.transform.lossyScale.x;
            // float height = parentCap.logObject.GetComponent<SpriteRenderer>().bounds.extents.y * parentCap.transform.lossyScale.y;
            float[] Xs = new float[] { -width, width };
            Vector2[] points;
            points = getDerivedLines(slopes[i], hmodX[i], hmodY[i], Xs, Vector2.zero);
            //points[0] = points[0] / 9.7f;
            //points[1] = points[1] / 10.4f;
            LINERENDER_DEBUG_DRAW_LINES(points[0], points[1], i);
        }
        // lineMaster.drawer.DEBUG_DRAW_LOG_LINES
    }

    public void LINERENDER_DEBUG_DRAW_LINES(Vector2 point1, Vector2 point2, int index) {

        ///
        /// add some kind of flair to lines appearing= either aggressively paint them on
        /// or have them pop up by expanding
        ///

        float planeWidth = parentCap.matAdjust.transform.localScale.x;
        float planeHeight = parentCap.matAdjust.transform.localScale.z;
        float ratio = planeWidth / planeHeight;
        // Debug.Log("Ratio: " + ratio);

        // gets the ratio between the render text (screen) and it's parent.
        float xRatio = parentCap.matAdjust.transform.localScale.x / parentCap.transform.localScale.x;
        float yRatio = parentCap.matAdjust.transform.localScale.z / parentCap.transform.localScale.y;

        lineTrans.localScale = new Vector3(xRatio, xRatio, lineTrans.localScale.z);
        //lineTrans.localScale = new Vector3(ratio, ratio, ratio);

        point1 = new Vector2(point1.x, point1.y);
        point2 = new Vector2(point2.x, point2.y);

        //point1 = new Vector2(point1.x, point1.y);
        //point2 = new Vector2(point2.x, point2.y);

        //Transform parentTrans = parentCap.logObject.transform;

        //point1 = parentTrans.TransformPoint(point1);
        //point2 = parentTrans.TransformPoint(point2);

        //point1 -= new Vector2(parentTrans.position.x, parentTrans.position.y);
        //point1 -= new Vector2(parentTrans.position.x, parentTrans.position.y);

        //point1 /= parentTrans.parent.localScale.x;
        //point2 /= parentTrans.parent.localScale.x;

        chopLineSpawnerTransform.GetChild(index).gameObject.GetComponent<LineRenderer>().SetPosition(0, point1);
        chopLineSpawnerTransform.GetChild(index).gameObject.GetComponent<LineRenderer>().SetPosition(1, point2);

        if(!hasSetSpacing && parentCap != null) {
            DEBUGSETUPDOTSPACING();
            hasSetSpacing = true;
        }
        // chopLineSpawnerTransform.GetChild(index).gameObject.GetComponent<line_render_control>().setDotSpacing(point1, point2, 10);
        chopLineSpawnerTransform.GetChild(index).gameObject.GetComponent<line_render_control>().setScroll(new Vector2(2, 0));
        //foreach (Transform child in chopLineSpawnerTransform) {
        // if(child.gameObject.activeInHierarchy) {
        //    child.gameObject.GetComponent<LineRenderer>().SetPosition(0, point1);
        //    child.gameObject.GetComponent<LineRenderer>().SetPosition(1, point2);
        //    }
        //}
    }

    void DEBUGSETUPDOTSPACING() {
 
        for (int i = 0; i < slopes.Length; i++) {
            float width = parentCap.logObject.GetComponent<SpriteRenderer>().bounds.extents.x * parentCap.transform.lossyScale.x;
            // float height = parentCap.logObject.GetComponent<SpriteRenderer>().bounds.extents.y * parentCap.transform.lossyScale.y;
            float[] Xs = new float[] { -width, width };
            Vector2[] points;
            points = getDerivedLines(slopes[i], hmodX[i], hmodY[i], Xs, Vector2.zero);
            chopLineSpawnerTransform.GetChild(i).gameObject.GetComponent<line_render_control>().setDotSpacing(points[0], points[1], 10);
        }
    }

    // Update is called once per frame
    void Update () {
        DEUBUG_DRAW();
	}
}
