﻿using UnityEngine;
using System.Collections;

///
/// actually draws lines
///

public class LogLineDrawer : MonoBehaviour {

    public Director director;
    public LogLineMaster lineMaster;

	// Use this for initialization
	void Start () {
	
	}

    public Vector2[] DEBUG_DRAW_LOG_LINES(float slope, float Xoffset, float Yoffset, Vector2 worldspace) {
        Vector2 point1, point2;
        float x1 = 100;
        float x2 = -100;
        float y1 = x1 * slope;
        float y2 = x2 * slope;
        //point1 = new Vector2(x1 + Xoffset, y1 + Yoffset) + worldspace;
        //point2 = new Vector2(x2 + Xoffset, y2 + Yoffset) + worldspace;
        point1 = new Vector2(x1 + Xoffset, y1 + Yoffset);
        point2 = new Vector2(x2 + Xoffset, y2 + Yoffset);
        Debug.DrawLine(point1, point2);

        return new Vector2[] { point1, point2 };
        // LINERENDER_DEBUG_DRAW_LINES(point1, point2);
    }


    // Update is called once per frame
    void Update () {
	
	}
}
