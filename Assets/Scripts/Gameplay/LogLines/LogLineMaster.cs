﻿using UnityEngine;
using System.Collections;

public class LogLineMaster : MonoBehaviour {

    // public static int maxNumLines = 3;

    public static GameObject chopLineRenderer;
    public ChopLineRenderSpawner lineRenderSpawner;
    public LogSpawner logSpawner;
    public Director director;
    public LogLineDrawer drawer;

	// Use this for initialization
	void Start () {
        
	}

    public void setup() {
        // lineRenderSpawner.init(this, maxNumLines);
    }

    // sets up random lines, enables lineRender objects.
    public void drawLinesOnLog(GameObject Log, int numLines, float lineWidth) {

        // float lineWidth     = director.GetComponent<GameController>().lineWidth;
        float[] angles      = new float[numLines];

        // local movement
        float[] offsetXs     = new float[numLines];
        float[] offsetYs     = new float[numLines];

        // world movement
        float[] worldOffsetXs = new float[numLines];
        float[] worldOffsetYs = new float[numLines];

        GameObject logObject = Log.GetComponent<LogCapsule>().logObject;

        for (int i = 0; i < numLines; i++) {

            // ratios that the plane is currently using
            //float planeWidth = Log.GetComponent<LogCapsule>().matAdjust.meshWidth;
            //float planeHeight = Log.GetComponent<LogCapsule>().matAdjust.meshHeight;
            //float ratio = planeWidth / planeHeight;

            // gets the world pos of the log
            Vector3 localToWorldOffsetPoint = logObject.transform.position;

            // Vector3 localToWorldOffsetPoint = logObject.transform.TransformPoint(logObject.transform.localPosition);
            // gets a delta movement in worldspace of the given offset on X and Y
            float worldOffsetX = genRandOffset(director.GetComponent<GameController>().lineOffsetRange) + localToWorldOffsetPoint.x;
            float worldOffsetY = genRandOffset(director.GetComponent<GameController>().lineOffsetRange) + localToWorldOffsetPoint.y;

            // worldOffsetX *= ratio;

            // converts the delta move in world to local space as a vector3
            localToWorldOffsetPoint = logObject.transform.InverseTransformPoint(new Vector3(worldOffsetX, worldOffsetY, logObject.transform.position.z));

            //localToWorldOffsetPoint = new Vector3(
            //    genRandOffset(director.GetComponent<GameController>().lineOffsetRange), 
            //    genRandOffset(director.GetComponent<GameController>().lineOffsetRange), 
            //    logObject.transform.position.z);

            // DEBUG
            // localToWorldOffsetPoint = new Vector3(genRandOffset(director.GetComponent<GameController>().lineOffsetRange), genRandOffset(director.GetComponent<GameController>().lineOffsetRange), 1);

            angles[i] = Mathf.Cos(genRandAngle(director.GetComponent<GameController>().lineAngleRange));
            offsetXs[i] = localToWorldOffsetPoint.x;
            offsetYs[i] = localToWorldOffsetPoint.y;
            worldOffsetXs[i] = worldOffsetX;
            worldOffsetYs[i] = worldOffsetY;
        }


        logObject.GetComponent<LogLineStore>().setLines(angles, offsetXs, offsetYs, worldOffsetXs, worldOffsetYs ,lineWidth);

        // resets lineRenderer, specifying how many renderers there are, and 
        // how wide they will be
        logObject.GetComponent<logBehavior>().chopLineSpawner.reset(numLines, lineWidth);

        logSpawner.randomRot(logObject);
        // Debug.Log(numLines);
    }

    public float genRandAngle(float range) {
        return Random.Range(-range, range);
    }

    public float genRandOffset(float range) {
        return Random.Range(-range, range);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
