﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChopVector : MonoBehaviour {

    // the control that manages this vector
    public ChopPointControl chopControl;

    // parent dot to this vector
    public GameObject parent;

    // public static float moveSpeed = 15f;

    // object is set to be dragged on spawn. 
    // Cannot be moved once let go.
    public bool isDragging;

    // Use this for initialization
    void Start() {

        // the user is currently dragging a dot
        // (dragging starts on instantiation)
        chopControl.isPlacingDot = true;
        isDragging = true;
    }

    public void moveToStartPos() {
        transform.position = chopControl.director.cam.ScreenToWorldPoint(Input.GetTouch(0).position);
    }

    void moveToTouch() {
        if(Input.touchCount > 0) {

            Vector3 screenPoint = chopControl.director.cam.ScreenToWorldPoint(Input.GetTouch(0).position);
            Vector3 pointTo = new Vector3(screenPoint.x, screenPoint.y, 0);

            // move towards the location of the first touch
            transform.position = Vector3.MoveTowards(
                transform.position, 
                pointTo,
                chopControl.director.dotTrackSpeed);

        }
        else {

            // adds a chop to the chopCounter
            ChopCounter.addChop();

            // user has finished placing this dot
            chopControl.isPlacingDot = false;

            // dot will not move any more
            isDragging = false;

            // adds the locations to the chopControl
            addPoints();
        }
    }

    // adds the locations of the vector and parent as chop information
    void addPoints() {
        List<Vector3> points = new List<Vector3>();

        float slope = 
            (transform.position.y - parent.transform.position.y) / 
            (transform.position.x - parent.transform.position.x);

        // arbitrarily long X based on starting X
        // NIGGERINO
        float newX =
            transform.position.x +
            chopControl.director.cam.orthographicSize *
            100;

        // X intercept
        float hmod = -1 * (slope * transform.position.x) - transform.position.y;

        float newY = (newX * slope) + hmod;


        Vector3 extendVectorPoint = new Vector3(
            newX ,
            newY ,
            transform.position.z
            );

        points.Add(transform.position);
        points.Add(parent.transform.position);
        chopControl.addPointsToList(points);
    }
	
	// Update is called once per frame
	void Update () {
        if (isDragging) {
            moveToTouch();
        }
	}
}
