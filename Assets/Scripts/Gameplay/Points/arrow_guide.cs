﻿using UnityEngine;
using System.Collections;

public class arrow_guide : MonoBehaviour {

    public ChopParent parent;
    public Vector3 dest;

	// Use this for initialization
	void Start () {
	
	}

    void moveToPoint() {
        Vector3 dotPoint = dest;
        Vector3 pointTo = new Vector3(dotPoint.x, dotPoint.y, 0);

        // move towards the location of the first touch
        transform.position = Vector3.MoveTowards(
            transform.position,
            pointTo,
            parent.chopControl.director.arrowGuideSpeed);
    }
	
	// Update is called once per frame
	void Update () {
        moveToPoint();
	}
}
