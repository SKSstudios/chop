﻿using UnityEngine;
using System.Collections;

public class ChopParent : MonoBehaviour {


    public ParticleSystem sparks;

    // the control that manages this vector
    public ChopPointControl chopControl;

    // vector dot to this parent
    public GameObject vector;

	// Use this for initialization
	void Start () {
        setupLines();
        delayLineBuf();
    }

    public void reset() {
        GetComponent<line_render_control>().enable();
        GetComponent<ChopParentAnim>().reset();
        setupLines();
    }

    void setupLines() {
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<line_render_control>().setScroll(new Vector2(chopControl.director.arrowGuideSpeed, 0));
        GetComponent<line_render_control>().setRenderOrder("Line");
    }

    // delays the lineRenderer by 1 frame to allow sync movements to occur
    private void delayLineBuf() {
        GetComponent<LineRenderer>().enabled = false;
        StartCoroutine(frameDelay());
    }

    IEnumerator frameDelay() {
        yield return new WaitForEndOfFrame();
        GetComponent<LineRenderer>().enabled = true;
    }

    // redraws the guide for the chop
    void drawGuide() {

        // only moves if the vector is moving
        if(vector.GetComponent<ChopVector>().isDragging) {
            GetComponent<LineRenderer>().SetPosition(1, vector.transform.position);
        }
    }

    // sets the scale of the chop guide
    void setGuideScale() {
        GetComponent<line_render_control>().setDotSpacing(transform.position, vector.transform.position, 20);
        // GetComponent<line_render_control>().setTileSize(chopControl.director.guideScale,0);
    }

    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.gameObject.tag.Equals("Log")) {
            sparks.Play();
        }
        else if (coll.gameObject.tag.Equals("Dot") && !vector.GetComponent<ChopVector>().isDragging && GameController.chopping) {
            vector.SetActive(false);
            gameObject.SetActive(false);
        }
        else {
            sparks.Stop();
        }
    }

    // Update is called once per frame
    void Update () {
        drawGuide();
        setGuideScale();
    }
}
