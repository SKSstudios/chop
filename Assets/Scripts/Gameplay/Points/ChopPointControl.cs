﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChopPointControl : MonoBehaviour {

    // how many chops until the log will attempt
    // to be split
    // public float numChops;

    // speed that the parent point will accelerate
    public float chopSpeed;

    // scripts
    public ChopCounter counter;
    public Director director;
    public GameController controller;

    // if the user is currently placing the vector dot
    public bool isPlacingDot;

    // dots in scene.
    public List<GameObject> dotParents;
    public List<GameObject> dotVectors;

    // strikes of the chops.
    // a successful chop is within line tolerances
    List<List<Vector3>> points;

	// Use this for initialization
	void Start () {
       
	}

    // sets up variables
    public void setup() {
        isPlacingDot = false;
        dotParents = new List<GameObject>();
        dotVectors = new List<GameObject>();

        counter = new ChopCounter();
        resetPointList();
        chopSpeed = director.chopSpeed;
    }

    // creates two dots, which will attempt to perform
    // a slice
    public void createSlice(Vector3 initPos) {

        Vector3 position = new Vector3(initPos.x, initPos.y, 0);

        // retrieves dot objects from buffer
        GameObject newParent = director.resourceMaster.parentDotBuffer.getObject(director.dotScale, position);
        GameObject newVector = director.resourceMaster.vectorDotBuffer.getObject(director.dotScale, position);

        // add the new objects to the list of active dots
        dotParents.Add(newParent);
        dotVectors.Add(newVector);

        // references the objects to each other
        newParent.GetComponent<ChopParent>().vector = newVector;
        newVector.GetComponent<ChopVector>().parent = newParent;

        // sets the chop controls of the new dots
        newParent.GetComponent<ChopParent>().chopControl = this;
        newVector.GetComponent<ChopVector>().chopControl = this;

        // resets the parent component
        newParent.GetComponent<ChopParent>().reset();

        newVector.GetComponent<ChopVector>().moveToStartPos();
    }

    // checks to see if the current number of chops is equal to
    // the number of chops required to start
    void checkChops() {

        if(ChopCounter.chopCount >= ChopCounter.numChops) {

            // sets the log to be chopped
            // controller.setLog(gameObject);

            // validates and sets chops
            controller.validateChops(points);

            // starts the anims which will lead to callbacks
            startParentChopAnims();
        }
    }

    // starts the chop animations
    void startParentChopAnims() {
        //for (int i = 0; i < dotParents.Count; i++) {
        //    dotParents[i].GetComponent<line_render_control>().disable();
        //    dotParents[i].GetComponent<ChopParentAnim>().startSliding();
        //}
        director.coRoutineMaster.delayChopTimesRoutine(dotParents, director);
    }

    // adds a pair of points to the list.
    public void addPointsToList(List<Vector3> newPoints) {
        director.countDisplay.newChop((int)ChopCounter.chopCount - 1);
        points.Add(newPoints);
        checkChops();
    }

    public void setNumChops(int numchops) {
        ChopCounter.numChops = numchops;
    }

    // resets the point list.
    public void resetPointList() {
        points = new List<List<Vector3>>();
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
