﻿using UnityEngine;
using System.Collections;

public class ChopParentAnim : MonoBehaviour {

    public float lerpTime, currentLerp;

    bool isSliding;

    // Use this for initialization
    void Start () {
	
	}

    public void reset() {
        isSliding = false;
        currentLerp = 0f;
        lerpTime = GetComponent<ChopParent>().chopControl.chopSpeed;
    }

    public void startSliding() {
        isSliding = true;
    }

    private void lerpToVector() {
        currentLerp += Time.deltaTime;
        float lerpStep = currentLerp / lerpTime;
        float newX = Mathfx.Sinerp(transform.position.x, GetComponent<ChopParent>().vector.transform.position.x, lerpStep);
        float newY = Mathfx.Sinerp(transform.position.y, GetComponent<ChopParent>().vector.transform.position.y, lerpStep);
        Vector3 lerpPos = new Vector3(newX, newY, transform.position.z);
        transform.position = lerpPos;   
    }


    // Update is called once per frame
    void Update () {
	    if(isSliding) {
            lerpToVector();
        }
	}
}
