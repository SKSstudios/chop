﻿using UnityEngine;
using System.Collections;

public class ChopCounter : MonoBehaviour {

    // numchops is the number of chops the user has.
    // chopcount is their current chop count.
    public static float numChops, chopCount;

	// Use this for initialization
	void Start () {
	
	}

    public static void addChop() {
        chopCount++;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
