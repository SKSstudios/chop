﻿using UnityEngine;
using System.Collections;

public class line_render_control : MonoBehaviour {

    public Texture texture;

    Vector2 deltaScroll;
    bool isScrolling;
    public Renderer lineRender;

    public float width;

	// Use this for initialization
	void Start () {
        setMatTexture();
	}

    public void setMatTexture() {
        // lineRender.material.SetTexture(0, texture);
        lineRender.material.mainTexture = texture;
    }

    public void setWidth(float width) {
        LineRenderer tempRend = (LineRenderer)lineRender;
        tempRend.SetWidth(width, width);
    }

    public void setRenderOrder(string layer) {
        lineRender.sortingLayerName = layer;
        // lineRender.sortingOrder = -1;
    }

    // sets dot spacing between two points
    public void setDotSpacing(Vector2 start, Vector2 end, float frequency) {

        // Debug.Log(lineRender.material);
        // Debug.Log(lineRender.material.mainTexture);
        float distance = Vector2.Distance(start, end) / frequency;
        lineRender.material.mainTexture.wrapMode = TextureWrapMode.Repeat;
        lineRender.material.mainTextureScale = new Vector3(distance, 1, 1);
        lineRender.sortingOrder = 0;

        // // ensures the tiles are wrapping
        // lineRender.materials[0].mainTexture.wrapMode = TextureWrapMode.Repeat;

        //// sets the scale of the texture to the distance
        //float distance = Vector2.Distance(start, end) / frequency;
        //lineRender.materials[0].mainTextureScale = new Vector3(distance, 1, 1);
    }

    public void setTileSize(Vector3 tileSize, int index) {

        // ensures the tiles are wrapping
        lineRender.materials[index].mainTexture.wrapMode = TextureWrapMode.Repeat;
        lineRender.materials[index].mainTextureScale = tileSize;
    }

    public void setScroll(Vector2 deltaMove) {
        deltaScroll = deltaMove;
        isScrolling = true;
    }

    private void scroll() {
        float xOffset   = 1 * Time.time * deltaScroll.x;
        float yOffset   = 1 * Time.time * deltaScroll.y;

        lineRender.material.mainTextureOffset = new Vector2(xOffset, yOffset);
    }

    public void setColor(Color color, int index) {
        lineRender.materials[index].color = color;
    }

    public void disable() {
        lineRender.enabled = false;
    }

    public void enable() {
        lineRender.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
	    if(isScrolling) {
            scroll();
        }
	}
}
