﻿using UnityEngine;
using System.Collections;

public class render_sorting_layer : MonoBehaviour {

    public Renderer _renderer;
    public string sortingLayer;

	// Use this for initialization
	void Start () {
        _renderer.sortingLayerName = sortingLayer;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
