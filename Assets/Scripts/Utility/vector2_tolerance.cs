﻿using UnityEngine;
using System.Collections;

public class vector2_tolerance : MonoBehaviour {

    private Vector2 point1, point2;
    private float tolerance,slope;

	// Use this for initialization
	void Start () {
	
	}

    public void setPoints(Vector2 point1, Vector2 point2) {

        float y1 = point1.y;
        float y2 = point2.y;
        float x1 = point1.x;
        float x2 = point2.x;

        this.point1 = point1;
        this.point2 = point2;

        this.slope = (y1 - y2) / (x1 - x2);
    }

    public Vector2 getPoint(float x) {
        return Vector2.zero;
    }

    public void setTolerance(float tolerance) {
        this.tolerance = tolerance;
    }

    public float contains(Vector2 testing) {
        //float newY = testing.x 
        // Vector2 newPoint = testing.x 
        return 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
