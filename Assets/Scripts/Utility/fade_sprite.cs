﻿using UnityEngine;
using System.Collections;

public class fade_sprite : MonoBehaviour {


    public SpriteRenderer sprite;

    // objects to be disabled/destroyed on fade
    public GameObject[] modifiedObjects;

    // is the sprite destroyed / disabled on fade?
    public bool isDestroyed, isDisabled, isShrinking;

    public bool fadeInOnCreation, fadeOutOnCreation, willShrink, hasRandomStartSize;

    // how much should the sprite fade in / out to
    public float fadeOutTo, fadeInTo;

    // time it takes for this sprite to transfer
    public float transferTime, minScale, deltaRandScaleMult;

    private bool isFadingOut, isFadingIn;
    private float lerpTime;
    private Vector3 initScale, finScale;

	// Use this for initialization
	void Start () {
        if (hasRandomStartSize)
            transform.localScale *= 1 + Random.Range(-deltaRandScaleMult, deltaRandScaleMult);
        if (fadeInOnCreation)
            startFadeIn();
        if (fadeOutOnCreation)
            startFadeOut();
        if (willShrink)
            startShrink();
	}

    public void startFadeIn() {
        isFadingIn = true;
        isFadingOut = false;
        lerpTime = 0;
    }

    public void startFadeOut() {
        isFadingOut = true;
        isFadingIn = false;
        lerpTime = 0;
    }

    public void startShrink() {
        isShrinking = true;
        initScale = transform.localScale;

        if (minScale > 0)
            finScale = initScale * minScale;
        else
            finScale = Vector3.zero;
    }

    private float lerpStep(float time) {
        lerpTime += Time.deltaTime;
        float lerpStep = lerpTime / transferTime;
        return lerpStep;
    }
    
    private void shrinkStep(float lerpStep) {
        gameObject.transform.localScale = new Vector3(
            Mathf.Lerp(initScale.x, finScale.x, lerpStep),
            Mathf.Lerp(initScale.y, finScale.y, lerpStep),
            Mathf.Lerp(initScale.z, finScale.z, lerpStep)
        );
    }

    private void fadeOutStep() {
        Color spColor = sprite.color;
        float lerpStepVal = lerpStep(lerpTime);

        sprite.color = new Color(
            spColor.r,
            spColor.b,
            spColor.g,
            Mathfx.Coserp(
                fadeInTo, 
                fadeOutTo,
                lerpStepVal
            )
        );

        if (isShrinking)
            shrinkStep(lerpStepVal);

        modifyCheck(lerpStepVal);

    }

    private void fadeInStep() {
        Color spColor = sprite.color;
        float lerpStepVal = lerpStep(lerpTime);

        sprite.color = new Color(
            spColor.r,
            spColor.b,
            spColor.g,
            Mathfx.Coserp(
                fadeOutTo,
                fadeInTo,
                lerpStepVal
            )
        );

        modifyCheck(lerpStepVal);
    }

    private void modifyCheck(float lerpStepVal) {
        if (lerpStepVal > 1) {
            if (isDestroyed) {
                foreach (GameObject obj in modifiedObjects) {
                    Destroy(obj);
                }
            }
            if (isDisabled) {
                foreach (GameObject obj in modifiedObjects) {
                    obj.SetActive(false);
                }
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if (isFadingIn)
            fadeInStep();
        if (isFadingOut)
            fadeOutStep();
	}
}
