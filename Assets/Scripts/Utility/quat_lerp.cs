﻿using UnityEngine;
using System.Collections;

public class quat_lerp : MonoBehaviour {

    public Transform moving, target;
    public float rotTime;

    public bool isRotating, rotateUsingSlerpSin, rotateUsingSlerpCos;

    private Quaternion initRot;
    private float lerpTime;

    // Use this for initialization
    void Start () {
	
	}

    public void startRotateSlerpSin() {
        isRotating = true;
        rotateUsingSlerpSin = true;
        rotateUsingSlerpCos = false;
        lerpTime = 0;
        initRot = transform.rotation;
    }

    public void startRotateSlerpCos() {
        isRotating = true;
        rotateUsingSlerpCos = true;
        rotateUsingSlerpSin = false;
        lerpTime = 0;
        initRot = transform.rotation;
    }

    // rotates using Quaternion slerp + sinerp smoothing
    void rotateSlerpSin() {
        lerpTime += Time.deltaTime;

        if (lerpTime > rotTime)
            return;

        float lerpStep = lerpTime / rotTime;
        // lerpStep = Mathf.Sin(lerpStep * Mathf.PI * 0.5f);
        lerpStep = Mathfx.Sinerp(0, 1, lerpStep);
        moving.rotation = Quaternion.Lerp(initRot, target.rotation, lerpStep);
        //float lerpStep = lerpTime / rotTime;

        //moving.rotation = Quaternion.Slerp(initRot, target.rotation, Mathfx.Sinerp(lerpStep, 1, lerpStep));
    }

    // rotates using Quaternion slerp + coserp smoothing
    void rotateSlerpCos() {
        lerpTime += Time.deltaTime;

        if (lerpTime > rotTime)
            return;

        float lerpStep = lerpTime / rotTime;
        // lerpStep = 1f - Mathf.Cos(lerpStep * Mathf.PI * 0.5f);
        lerpStep = Mathfx.Coserp(0, 1, lerpStep);
        moving.rotation = Quaternion.Lerp(initRot, target.rotation, lerpStep);
        //float lerpStep = lerpTime / rotTime;

        //moving.rotation = Quaternion.Slerp(initRot, target.rotation, Mathfx.Coserp(lerpStep, 1, lerpStep));
    }

    // Update is called once per frame
    void Update () {
	    if(isRotating) {
            if(rotateUsingSlerpSin) {
                rotateSlerpSin();
            }

            if(rotateUsingSlerpCos) {
                rotateSlerpCos();
            }
        }
	}
}
