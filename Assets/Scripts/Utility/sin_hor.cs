﻿using UnityEngine;
using System.Collections;

public class sin_hor : MonoBehaviour {

    public float frequency;
    public float amplitude;
    public bool isRandomStart, isMoving;

    private float timeSin;
    private float randomOffset;
    private Vector3 startpos;

    // Use this for initialization
    void Start() {

        // if this item's starting phase is to be randomized,
        // set randomOffset.
        if (isRandomStart) {
            randomOffset = Random.Range(0, 2);
        }

        isMoving = true;
        startpos = GetComponent<Transform>().localPosition;
    }

    Vector3 sin(Vector3 trans) {
        float timeDif = (timeSin - Time.time + randomOffset);
        float theta = timeDif / (frequency * 2);
        float distance = amplitude * Mathf.Sin(theta);
        trans = startpos + (Vector3.left * distance);
        trans = new Vector3(
            transform.localPosition.x,
            trans.y,
            trans.z);
        return trans;
    }



    // Update is called once per frame
    void Update() {

        if (isMoving) {
            transform.localPosition = sin(transform.localPosition);
        }

    }
}
