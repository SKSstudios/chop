﻿using UnityEngine;
using System.Collections;

public class rippedWheelRotation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    private float baseAngle = 0.0f;

    void OnMouseDown() {
        var dir = Camera.main.WorldToScreenPoint(transform.position);
        dir = Input.mousePosition - dir;
        baseAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
    }

    void OnMouseDrag() {
        var dir = Camera.main.WorldToScreenPoint(transform.position);
        dir = Input.mousePosition - dir;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - baseAngle;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
