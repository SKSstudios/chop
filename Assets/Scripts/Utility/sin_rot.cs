﻿using UnityEngine;
using System.Collections;

public class sin_rot : MonoBehaviour {

    public float frequency;
    public float amplitude;
    public bool isRandomStart, isMoving;

    private float timeSin;
    private float randomOffset;

	// Use this for initialization
	void Start () {

        // if this item's starting phase is to be randomized,
        // set randomOffset.
	    if(isRandomStart) {
            randomOffset = Random.Range(0, 360);
        }

        isMoving = true;
    }

    Transform rot(Transform trans) {
        float timeDif = (timeSin - Time.time + randomOffset);
        float theta = timeDif / (frequency * 2);
        float distance = amplitude * Mathf.Sin(theta);
        trans.Rotate(trans.rotation.x, trans.rotation.y, distance, Space.Self);
        return trans;
    }


	
	// Update is called once per frame
	void Update () {
	    if(isMoving) {
            GetComponent<Transform>().rotation = rot(transform).rotation;
        }
	}
}
