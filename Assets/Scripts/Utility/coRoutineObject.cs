﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
///
/// hosts coRoutines for scripts without gameObjects
///
public class coRoutineObject : MonoBehaviour {

    public GameController control;
    public Director director;

	// Use this for initialization
	void Start () {
	
	}

    // starts routine that moves the chop indicators, before calling back to gameController
    // to explode the log.
    // shakes screen with each chop, controlled from director.
    public void delayChopTimesRoutine(List<GameObject> dotParents, Director director) {
        StartCoroutine(delayChopTimes(dotParents, director));
    }

    public IEnumerator delayChopTimes(
        List<GameObject> dotParents,
        Director director) {
        for (int i = 0; i < dotParents.Count; i++) {
            yield return new WaitForSeconds(director.chopDelay);
            dotParents[i].GetComponent<line_render_control>().disable();
            dotParents[i].GetComponent<ChopParentAnim>().startSliding();
            director.shakeChop();
        }
        yield return new WaitForSeconds(director.chopSpeed * 2);
        control.coRoutineCallbackInitSlicing();
    }

    public void DEBUG_INTRO() {
        director.bearAnimControl.runAnims();
    }

    public void DEBUG_INTRO_CALLBACKCOMPLETE() {
        StartCoroutine(DEBUG_INTRO_CALLBACKCOMPLETE_COROUTINE());
    }

    public IEnumerator DEBUG_INTRO_CALLBACKCOMPLETE_COROUTINE() {
        director.startButtonZoom();
        // yield return new WaitForSeconds(1f);

        yield return new WaitForSeconds(0.7f);
        director.countDisplay.resetToCurrentChopNum();
        yield return new WaitForSeconds(1f);
        control.DEBUG_SPAWN_LINED_LOG();
        // director.logSpawn.spawnLog();
        yield return new WaitForSeconds(5f);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
