﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CounterDisplay : MonoBehaviour {


    public Director director;

    // time (s) for a transparent dot to pop in
    public float popInTimeTrans, popInTimeVis;
    public float initScale;
    public float popDelay;

    // images used in the counter
    public Image[] visibleDots;
    public Image[] transDots;

    // init positions of the 3 dots.
    private Transform[] dotPositions;
    private Vector3[] dotLocationsCopy;

	// Use this for initialization
	void Start () {
	
	}

    public void resetToCurrentChopNum() {
        reset((int)ChopCounter.numChops);
    }

    public void setup() {

        // sets up the init positions of the dots
        dotPositions = new Transform[transDots.Length];
        dotLocationsCopy = new Vector3[transDots.Length];
        
        // captures the dot's positions and makes them invisible
        for(int i = 0; i < dotPositions.Length; i++) {
            dotPositions[i] = visibleDots[i].gameObject.transform;
            dotLocationsCopy[i] = visibleDots[i].gameObject.transform.localPosition;

            // attatches animation scripts, makes them invisible.
            visibleDots[i].gameObject.AddComponent<CounterAnims>();
            visibleDots[i].GetComponent<CounterAnims>().countDisplay = this;
            visibleDots[i].GetComponent<CounterAnims>().lerpTime = popInTimeVis;
            visibleDots[i].GetComponent<CounterAnims>().initPopSize = initScale;
            visibleDots[i].GetComponent<CounterAnims>().dotImage = visibleDots[i];
            visibleDots[i].GetComponent<CounterAnims>().dotTrans = visibleDots[i].transform;
            visibleDots[i].GetComponent<CounterAnims>().maxSizeTrans = visibleDots[i].transform.localScale;
            visibleDots[i].GetComponent<CounterAnims>().reset();

            transDots[i].gameObject.AddComponent<CounterAnims>();
            transDots[i].GetComponent<CounterAnims>().countDisplay = this;
            transDots[i].GetComponent<CounterAnims>().lerpTime = popInTimeTrans;
            transDots[i].GetComponent<CounterAnims>().initPopSize = initScale;
            transDots[i].GetComponent<CounterAnims>().dotImage = transDots[i];
            transDots[i].GetComponent<CounterAnims>().dotTrans = transDots[i].transform;
            transDots[i].GetComponent<CounterAnims>().maxSizeTrans = transDots[i].transform.localScale;
            transDots[i].GetComponent<CounterAnims>().reset();
        }

    }

    // resets the counter
    public void reset(int size) {

        List<CounterAnims> popInList = new List<CounterAnims>();

        // resets the position of the dots
        for (int i = 0; i < dotPositions.Length; i++) {
            visibleDots[i].transform.localPosition = dotLocationsCopy[i];
            transDots[i].transform.localPosition = dotLocationsCopy[i];
        }

        if(size == 1) {
            visibleDots[0].transform.localPosition = dotLocationsCopy[1];
            transDots[0].transform.localPosition = dotLocationsCopy[1];

            popInList.Add(transDots[0].GetComponent<CounterAnims>());
        }

        if(size == 2) {
            float offset = dotPositions[0].transform.localPosition.x;
            Vector3 offsetVec = new Vector3(offset, 0, 0);

            visibleDots[0].transform.localPosition = dotLocationsCopy[1] + offsetVec;
            transDots[0].transform.localPosition = dotLocationsCopy[1] + offsetVec;
            visibleDots[1].transform.localPosition = dotLocationsCopy[1] - offsetVec;
            transDots[1].transform.localPosition = dotLocationsCopy[1] - offsetVec;

            popInList.Add(transDots[0].GetComponent<CounterAnims>());
            popInList.Add(transDots[1].GetComponent<CounterAnims>());
        }

        if(size == 3) {
            //visibleDots[0].transform.position = dotPositions[0].transform.position;
            //transDots[0].transform.position = dotPositions[0].transform.position;
            //visibleDots[1].transform.position = dotPositions[1].transform.position;
            //transDots[1].transform.position = dotPositions[1].transform.position;
            //visibleDots[2].transform.position = dotPositions[2].transform.position;
            //transDots[2].transform.position = dotPositions[2].transform.position;

            popInList.Add(transDots[0].GetComponent<CounterAnims>());
            popInList.Add(transDots[1].GetComponent<CounterAnims>());
            popInList.Add(transDots[2].GetComponent<CounterAnims>());
        }

        StartCoroutine(popInDelay(popInList));

        // transDots[0].GetComponent<CounterAnims>().popCounterIn();
        // animControl.popCounterIn(transDots[0].transform);
    }



    // pops in slice counter
    public void newChop(int chopNum) {
        visibleDots[chopNum].GetComponent<CounterAnims>().popCounterIn();
    }

    IEnumerator popInDelay(List<CounterAnims> toPop) {
        for(int i = 0; i < toPop.Count; i++) {
            yield return new WaitForSeconds(popDelay);
            toPop[i].popCounterIn();
        }
    }
    
	// Update is called once per frame
	void Update () {
	
	}
}
