﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CounterAnims : MonoBehaviour {

    public CounterDisplay countDisplay;

    public Image dotImage;
    public Transform dotTrans;
    public Vector3 maxSizeTrans;

    // time for a pop in to occur
    public float lerpTime;
    private float currentlerp;

    // scale that the image pops from
    public float initPopSize;

    public bool isGrowing;
    public bool isShrinking;


	// Use this for initialization
	void Start () {
	
	}

    // resets this dot to it's invisible default state
    public void reset() {
        dotImage.transform.localScale = maxSizeTrans;
        dotImage.enabled = false;
        currentlerp = 0f;
    }

    // makes an invisible counter "pop" in.
    public void popCounterIn() {
        dotTrans.localScale *= initPopSize;
        dotImage.enabled = true;
        isGrowing = true;
        isShrinking = false;
    }

    private void lerpSizeUp() {
        currentlerp += Time.deltaTime;
        float lerpStep = currentlerp / lerpTime;
        dotTrans.localScale = Mathfx.Coserp(dotTrans.localScale, maxSizeTrans, lerpStep);
    }

    //private IEnumerator popIn(Transform popped) {

    //    //float wait = 0.01f;
    //    //float deltaChange = (1 - initPopSize) * wait;
    //    //Vector3 deltaVector3 = 
    //    //    new Vector3(
    //    //        deltaChange, 
    //    //        deltaChange, 0
    //    //        );

    //    //for(int i = 0; i < timePopIn / wait; i++) {
    //    //    yield return new WaitForSeconds(wait);
    //    //    popped.localScale += deltaVector3;
    //    //}

    //    while(popped.localScale != Vector3.one) {
    //        yield return new WaitForEndOfFrame();
    //        popped.localScale += 
    //    }
    //}
	
	// Update is called once per frame
	void Update () {
        if(isGrowing) {
            lerpSizeUp();
        }
	}
}
