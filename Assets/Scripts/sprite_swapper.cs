﻿using UnityEngine;
using System.Collections;

public class sprite_swapper : MonoBehaviour {

    public SpriteRenderer spRenderer;
    public Sprite[] sprites;

	// Use this for initialization
	void Start () {
        spRenderer = GetComponent<SpriteRenderer>();
	}

    public void setSprite(int indice){
        spRenderer.sprite = sprites[indice];
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
