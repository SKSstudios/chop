﻿using UnityEngine;
using System.Collections;

public class MaterialDimensionsAdjust : MonoBehaviour {

    public float meshHeight, meshWidth;
 
    public MeshFilter plane;
    public SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
	
	}

    public void setDimensions() {
        meshWidth = plane.mesh.bounds.extents.x;
        float spriteWidth = sprite.bounds.extents.x;
        float multRatioWidth = meshWidth * (spriteWidth / meshWidth) / 2;

        meshHeight = plane.mesh.bounds.extents.z;
        Debug.Log("Mesh Height: " + meshHeight * plane.gameObject.transform.localScale.z);
        float spriteHeight = sprite.bounds.extents.y;
        float multRatioHeight = meshHeight * (spriteHeight / meshHeight) / 2;

        plane.transform.localScale = new Vector3(multRatioWidth / 9, 1, multRatioHeight / 9);

        Debug.Log("Mesh Height: " + meshHeight * plane.gameObject.transform.localScale.z);
        Debug.Log("Sprite Height: " + sprite.bounds.extents.y);

        //   Mesh m = new Mesh();
        //   m.name = "logMesh";

        //   float height = sprite.bounds.size.y;
        //   float width = sprite.bounds.size.x;

        //   m.vertices = new Vector3[] {
        //    new Vector3(-width, -height, 0.01f),
        //    new Vector3(width, -height, 0.01f),
        //    new Vector3(width, height, 0.01f),
        //    new Vector3(-width, height, 0.01f)
        //};
        //   m.uv = new Vector2[] {
        //    new Vector2 (0, 0),
        //    new Vector2 (0, 1),
        //    new Vector2(1, 1),
        //    new Vector2 (1, 0)
        //};
        //   m.triangles = new int[] { 0, 1, 2, 0, 2, 3 };

        //   planeFilter.mesh = m;
        //   m.RecalculateNormals();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
