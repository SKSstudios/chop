﻿using UnityEngine;
using System.Collections;

public class bearAnimController : MonoBehaviour {

    public Director director;
    public ZSpawner zspawner;
    public GameObject bear, axe;

    public Sprite awake, asleep;

    // point 0 is the default location
    // points 1 and 2 are the anchors for the "pop up" animation when Teddy is woken up
    // points 3 and 4 are the anchors for the "rise" animation when Teddy gets ready to chop
    public Transform[] bearPoints, axePoints;

    public move_trans_to_vector3 bearMove, axeMove;
    public quat_lerp axeRotLerp;
    public bearBlink blink;

	// Use this for initialization
	void Start () {
        // init();
        // runAnims();
	}

    public void init() {
        bear.transform.position = bearPoints[0].position;
        axe.transform.position = axePoints[0].position;
        bear.GetComponent<SpriteRenderer>().sprite = asleep;
        bear.GetComponent<bearBlink>().isBlinking = false;
        zspawner.isSpawning = true;
    }

    public void runAnims() {
        wakeUp(0.1f, 0.15f, 0.15f, 1, 1, 1, 1, 0.7f, 1.5f);
    }

    // initdelay- initial delay before starting animation
    // popTime- time to pop up and down (startled)
    // riseUpTime- time to rise up and settle down
    public void wakeUp(float initDelay, 
        float popUpTimeBear, float popDownTimeBear,
        float popUpTimeAxe, float popDownTimeAxe, 
        float riseUpTimeBear, float riseDownTimeBear,
        float riseUpTimeAxe, float riseDownTimeAxe) {

        StartCoroutine(animationSequenceCaller(initDelay,
            popUpTimeBear, popDownTimeBear,
            popUpTimeAxe, popDownTimeAxe,
            riseUpTimeBear, riseDownTimeBear,
            riseUpTimeAxe, riseDownTimeAxe));

    }

    // calls animation sequences with provided time offsets.
    public IEnumerator animationSequenceCaller(float initDelay,
        float popUpTimeBear, float popDownTimeBear,
        float popUpTimeAxe, float popDownTimeAxe,
        float riseUpTimeBear, float riseDownTimeBear,
        float riseUpTimeAxe, float riseDownTimeAxe) {

        // starts the coroutines for animating the bear and the axe's "pop" movements
        StartCoroutine(
            popDelay(
                initDelay,
                popUpTimeBear,
                popDownTimeBear,
                popUpTimeAxe,
                popDownTimeAxe
                )
            );

        yield return new WaitForSeconds(initDelay + popUpTimeBear + popDownTimeBear + 0.2f);

        StartCoroutine(riseUpBear(riseUpTimeBear, riseDownTimeBear));
        StartCoroutine(riseUpAxe(riseUpTimeAxe, riseDownTimeAxe));

        StartCoroutine(ZoomOutCallback(0f));
    }

    public IEnumerator popDelay(float delay,
        float popUpTimeBear, float popDownTimeBear,
        float popUpTimeAxe, float popDownTimeAxe) {
        yield return new WaitForSeconds(delay);
        StartCoroutine(popUpBear(popUpTimeBear, popDownTimeBear));
    }

    // makes the bear "pop up"
    public IEnumerator popUpBear(float popUpTime, float popDownTime) {

        zspawner.removeZs();

        bearMove.targetTrans = bearPoints[1];
        bearMove.moveTime = popUpTime;
        bearMove.startSinerp();
        // bearMove.startCoserp();

        yield return new WaitForSeconds(popUpTime);

        bearMove.targetTrans = bearPoints[2];
        bearMove.moveTime = popDownTime;
        // bearMove.startSinerp();
        bearMove.startCoserp();

        StartCoroutine(wakeUpBlink());
    }

    public IEnumerator wakeUpBlink() {

        // opens eyes (bleary / confused)
        blink.manualSquint();
        yield return new WaitForSeconds(0.3f);

        for(int i = 0; i < 2; i++) {
            blink.manualCloseEyes();
            yield return new WaitForSeconds(0.07f);
            blink.manualOpenEyes();
            yield return new WaitForSeconds(0.3f);
        }

    }

    public IEnumerator riseUpBear(float riseUpTime, float riseDownTime) {
        bearMove.targetTrans = bearPoints[3];
        bearMove.moveTime = riseUpTime;
        bearMove.startSinerp();
        yield return new WaitForSeconds(riseUpTime);
        bearMove.targetTrans = bearPoints[4];
        bearMove.moveTime = riseDownTime;
        bearMove.startCoserp();
    }

    public IEnumerator riseUpAxe(float riseUpTime, float riseDownTime) {

        axeRotLerp.rotTime = riseUpTime;
        axeRotLerp.target = axePoints[3];
        axeRotLerp.startRotateSlerpSin();

        axeMove.targetTrans = axePoints[3];
        axeMove.moveTime = riseUpTime;
        axeMove.startSinerp();

        yield return new WaitForSeconds(riseUpTime);

        axeRotLerp.rotTime = riseDownTime;
        axeRotLerp.target = axePoints[4];
        axeRotLerp.startRotateSlerpCos();

        axeMove.targetTrans = axePoints[4];
        axeMove.moveTime = riseDownTime;
        axeMove.startCoserp();

    }

    public IEnumerator ZoomOutCallback(float delay) {
        yield return new WaitForSeconds(delay);
        director.zoomOutCallback();
    }



    // Update is called once per frame
    void Update () {
	
	}
}
