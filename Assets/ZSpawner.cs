﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZSpawner : MonoBehaviour {

    public GameObject zParentPrefab;
    public bool isSpawning;
    public float ZDelay, ZScale;
    public List<GameObject> zs;

    public Transform zTarget;

	// Use this for initialization
	void Start () {
        zs = new List<GameObject>();
        StartCoroutine(spawnZ());
	}

    public IEnumerator spawnZ() {
        newZ();
        while (true) {
            yield return new WaitForSeconds(ZDelay);
            if(isSpawning) {
                newZ();
            }
        }
    }

    public void newZ() {
        GameObject newZObject = Instantiate(zParentPrefab, transform.position, Quaternion.identity) as GameObject;
        newZObject.transform.localScale = new Vector3(ZScale, ZScale, ZScale);
        newZObject.GetComponent<move_trans_to_vector3>().targetTrans = zTarget;
        zs.Add(newZObject);
    }

    public void removeZs() {
        foreach(GameObject z in zs) {
            fade_sprite fSprite = z.transform.GetChild(0).GetComponent<fade_sprite>();
            fSprite.transferTime = 0.5f;
            fSprite.isDestroyed = true;
            fSprite.startFadeOut();

            // z.SetActive(false);
        }
        isSpawning = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
