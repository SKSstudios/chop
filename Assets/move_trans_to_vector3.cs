﻿using UnityEngine;
using System.Collections;

public class move_trans_to_vector3 : MonoBehaviour {


    public Transform targetTrans;

    // deltaMove if using moveTowards, moveTime if using lerps
    public float deltaMove, moveTime;

    public Transform moving;

    public Vector3 initPos;

    public bool isMoving, usesMoveTowards, usesSinerp, usesCoserp;

    private float lerpTime;


	// Use this for initialization
	void Start () {
        if (usesMoveTowards)
            startMoveTowards();
        if (usesCoserp)
            startCoserp();
        if (usesSinerp)
            startSinerp();
	}

    public void startMoveTowards() {
        usesSinerp = false;
        usesCoserp = false;
        usesMoveTowards = true;
        isMoving = true;
    }

    public void startSinerp() {
        usesSinerp = true;
        usesCoserp = false;
        usesMoveTowards = false;
        isMoving = true;
        initPos = moving.position;
        lerpTime = 0;
    }

    public void startCoserp() {
        usesSinerp = false;
        usesCoserp = true;
        usesMoveTowards = false;
        isMoving = true;
        initPos = moving.position;
        lerpTime = 0;
    }

    private void sinerpTo() {
        lerpTime += Time.deltaTime;
        if (lerpTime > moveTime)
            return;
        float lerpStep = lerpTime / moveTime;
        moving.position = new Vector3(
            Mathfx.Sinerp(initPos.x, targetTrans.position.x, lerpStep),
            Mathfx.Sinerp(initPos.y, targetTrans.position.y, lerpStep),
            Mathfx.Sinerp(initPos.z, targetTrans.position.z, lerpStep)
        );
    }
    
    private void coserpTo() {
        lerpTime += Time.deltaTime;
        if (lerpTime > moveTime)
            return;
        float lerpStep = lerpTime / moveTime;
        moving.position = new Vector3(
            Mathfx.Coserp(initPos.x, targetTrans.position.x, lerpStep),
            Mathfx.Coserp(initPos.y, targetTrans.position.y, lerpStep),
            Mathfx.Coserp(initPos.z, targetTrans.position.z, lerpStep)
        );
    }

    private void moveTowardsTarget() {
        moving.position = Vector3.MoveTowards(
            moving.position,
            targetTrans.position,
            deltaMove
        );
    }

    // Update is called once per frame
    void Update () {
        if(isMoving) {
            if(usesMoveTowards)
                moveTowardsTarget();
            if (usesCoserp)
                coserpTo();
            if (usesSinerp)
                sinerpTo();
        }
        
	}
}
