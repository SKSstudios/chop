﻿using UnityEngine;
using System.Collections;

public class drawRect : MonoBehaviour {

    public Camera mainCam;
    public Transform parent;
    public SpriteRenderer spRender;
    public Material mask;
    public RenderTexture renderTex;

	// Use this for initialization
	void Start () {
	
	}

    void OnGUI() {
        if (mainCam == null)
            return;

        Vector3 logPos = mainCam.WorldToScreenPoint(parent.position);
        logPos = new Vector3(logPos.x, logPos.y);
        float width = spRender.bounds.extents.x;
        float height = spRender.bounds.extents.y;
        Rect textureBoard = new Rect(logPos, new Vector2(width * 2, height * 2));
        GUI.DrawTexture(textureBoard, renderTex);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
