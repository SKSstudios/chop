﻿using UnityEngine;
using System.Collections;

public class renderCamAdjust : MonoBehaviour {

    public Camera renderCam;
    public SpriteRenderer logSprite;
    public RenderTexture texture;

	// Use this for initialization
	void Start () {
	
	}

    public void setDimensions() {
        float height = logSprite.bounds.size.y;
        float width = logSprite.bounds.size.x;
        renderCam.orthographicSize = height / 2;
        renderCam.aspect = (width / height);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
