﻿using UnityEngine;
using System.Collections;

///
/// controls the renderers and shaders that indicate where a chop may be performed
/// spawns them on the log object's child line master object
/// 
///

public class ChopLineRenderSpawner : MonoBehaviour {

    public static int maxNumLines = 3;

    // public LogLineMaster lineMaster;

	// Use this for initialization
	void Start () {
        // init(maxNumLines);
	}

    // initializes the object= spawning the maximal number
    // of lines and setting their parent to this transform.
    // they will be activated/deactivated as needed.
    public void init(int size) {
        // lineMaster = master;
        for (int i = 0; i < size; i++) {
            GameObject newChild = Instantiate<GameObject>(LogLineMaster.chopLineRenderer);
            newChild.transform.SetParent(transform);
            newChild.transform.localPosition = Vector2.zero;
            newChild.GetComponent<line_render_control>().setRenderOrder("particles");
        }
        // reset(0);
    }

    // resets the render components so that only the required
    // number is provided. Additionally, sets the width of the
    // renderers.
    public void reset(int size, float width) {
        // Debug.Log(size);
        foreach (Transform child in gameObject.transform) {
            // Debug.Log("disabled child");
            child.gameObject.SetActive(false);
        }

        int i = 0;
        foreach (Transform child in transform) {
            if (i > size)
                break;
            // Debug.Log("enabled child");
            // Debug.Log(size);
            child.gameObject.SetActive(true);
            child.GetComponent<line_render_control>().setWidth(width);
            i++;
        }
        //for(int i = 0; i < 1; i++) {
        //    transform.GetChild(i).gameObject.SetActive(true);
        //}
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
