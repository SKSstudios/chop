﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LogCapsule : MonoBehaviour {

    public logBehavior logObjectBehavior;
    public GameObject logObject;
    public drawRect drawRect;
    public MaterialDimensionsAdjust matAdjust;

	// Use this for initialization
	void Start () {
	
	}

    // used to collect splinter gameObjects.
    // relies on there being no other extra crap on the logCap
    public List<GameObject> cleanUpSplinters() {
        List<GameObject> retSplinterList = new List<GameObject>();
        foreach(Transform splinter in transform) {
            if(splinter.gameObject.activeInHierarchy)
                retSplinterList.Add(splinter.gameObject);
        }
        return retSplinterList;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
